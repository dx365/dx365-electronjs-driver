const dx = require("./dx_device.node");

const Device = dx.WrapperClass;

const device = new Device(null);

console.log("Waiting for device");

device.onDeviceConnectionChanged((error, result, meta) => {
  const deviceId = meta.id;
  if (result.isConnected) {
    console.log(`Device connected (Device ID: ${deviceId})`);
    const reqId = device.getUUID();
    device.selectDevice({
      id: deviceId,
      reqId,
      callback: () => {},
    });
  } else {
    console.log(`Device disconnected (Device ID: ${deviceId})`);
  }
});

device.onCartridgeStatusChanged((error, result, meta) => {
  const deviceId = meta.id;
  if (result.isInserted) {
    console.log("Cartridge inserted");
    readRfid(deviceId);
  } else {
    console.log("Cartridge removed");
  }
});

device.onReadNfcLftReady((error, result, meta) => {
  console.log("NFC read result: ", result);
  getConcentration(meta.id);
});

device.onConcentrationReady((error, result, meta) => {
  console.log("Concentration read result: ", result);
  device.unsubscribeFromRequestProgress({
    id: meta.id,
    reqId: meta.reqId,
    callback: () => {},
  });
});

function readRfid(id) {
  const coord = {
    // does not matter
    x_bias: 823,
    y_bias: 611,
    scale_factor: 0.019231,
  };
  const reqId = device.getUUID();
  device.readNFCLFT({
    id,
    coord,
    reqId,
    callback: () => {},
  });
}

function getConcentration(id) {
  const reqId = device.getUUID();
  subscribeToRequestProgress(id, reqId, 100);
  device.getConcentration({
    id,
    croppedImagePath: "./a.bmp",
    croppedImageRawPath: "./b.bmp",
    reqId,
    callback: () => {},
  });
}

function subscribeToRequestProgress(id, reqId, period) {
  device.subscribeToRequestProgress({
    id,
    reqId,
    period,
    callback: () => {},
    onProgress: (error, result, meta) => {
      console.log("Progress: ", result.progress);
    },
  });
}
